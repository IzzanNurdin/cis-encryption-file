package xts_aes_13;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Crypto_Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		// File file = new File("C:/Users/nurdi/eclipse-workspace/Crypto-2017/src/xts_aes_13/test.bin");
		byte[] fileData = readBytesFromFile("E:/IzzanNurdin/Documents/CIS/cis-encryption-file/xts_aes_13/test.bin");
		// FileInputStream in = new FileInputStream(file);
		// String content = "";
		// in.read(fileData);
		// for(byte b : fileData) {
		// 	content += getBits(b);
		// }
		// System.out.println(content);
		// in.close();
		for (int i = 0; i < fileData.length; i++) {
                System.out.print((char) fileData[i]);
            }
	}

	static byte[] readBytesFromFile(String filePath) {
		FileInputStream fis = null;
        byte[] bytesArray = null;

        try {
            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            fis = new FileInputStream(file);
            fis.read(bytesArray);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File not found!");
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return bytesArray;
	}
	
	static String getBits(byte b) {
		String result = "";
		for(int i = 0; i < 4; i++) {
			result += (b & (1<<i)) == 0 ? "0" : "1";
		}
		return result;
	}

}
