<h1>XTS-AES-256</h1>
<hr>
<h5>Author:</h5>
<li>
	<ol>Meitya Dianti</ol>
	<ol>Muhammad Izzan Nuruddin</ol>
	<ol>Rama Rahmatullah</ol>
</li>

<p>XTS-AES Implementation of 256 Bit in Java</p> <br>
<p>Authored by Meitya Dianti, Muhammad Izzan Nuruddin, and Rama Rahmatullah for Cryptography and Information Security assignment</p> <br>
<p>Using AES Library by Joan Daemen, Vincent Rijmen, and Lawrie Brown</p>