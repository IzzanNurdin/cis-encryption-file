import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.params.KeyParameter;

import util.ByteUtil;
import util.Generator;
import util.TestVector;

public class Main {

	private static String plainText, key, cipherText, tweakKey, dataUnit;
	private static BlockCipher cipher, tweakCipher;
	private static CipherParameters keyParam, tweakParam;
	private static XTS xts;
	private static byte[] plainTextByte, cipherTextByte, producedCipherTextByte, producedPlainTextByte;
	private static long dataUnitNumber;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		plainText = TestVector.getPlainText();
		key = TestVector.getKey();
		dataUnit = TestVector.getDataUnit();
		cipherText = TestVector.getCipherText();
		tweakKey = TestVector.getTweakKey();
		
		
		setUpAESCipher();
		
		dataUnitNumber = ByteUtil.loadInt64BE(ByteUtil.hexToBytes(dataUnit), 0);
		
		System.out.println("====================================================");
        System.out.println("IEEE 1619 test vector 4");
        System.out.println("Key               " + key);
        System.out.println("Tweak key:        " + tweakKey);
        System.out.println("Data unit number: " + dataUnitNumber);
        System.out.println("Plaintext:        " + plainText);
        System.out.println("Ciphertext:       " + cipherText);
        System.out.println("====================================================");
        System.out.println("Result");
        System.out.println("====================================================");

		
		
		//Call this method to encrypt
		//Result stored in producedCipherTextByte
		encryptXTSMode();
		
		System.out.println("Ciphertext:       " + ByteUtil.bytesToHex(producedCipherTextByte));
        if (ByteUtil.bytesToHex(producedCipherTextByte).equals(cipherText))
            System.out.println("Ciphertext matches IEEE 1619 test vector 4");
        else
            System.out.println("Ciphertext does not match IEEE 1619 test vector 4");
	
		//Call this method to decrypt
		//Result stored in producedPlainTextByte
		decryptXTSMode();
		
		System.out.println("Plaintext:        " + ByteUtil.bytesToHex(producedPlainTextByte));
        if (ByteUtil.bytesToHex(producedPlainTextByte).equals(plainText))
            System.out.println("Plaintext matches IEEE 1619 test vector 4");
        else
            System.out.println("Plaintext does not match IEEE 1619 test vector 4");
		
	}

	private static void setUpAESCipher() {
		// TODO Auto-generated method stub
		
		//Pre-process key and tweakKey
		keyParam = new KeyParameter(ByteUtil.hexToBytes(key));
		tweakParam = new KeyParameter(ByteUtil.hexToBytes(tweakKey));
		
		//Create AES object
		cipher = new AESFastEngine();
        tweakCipher = new AESFastEngine();
        
        //init tweak
		tweakCipher.init(true, tweakParam);
	}
	
	private static void encryptXTSMode() {
		
		setAESMode(true);
		
		//Set up XTS mode
		XTS xts = new XTS(cipher, tweakCipher);
        plainTextByte = ByteUtil.hexToBytes(plainText);
        producedCipherTextByte = new byte[xts.getDataUnitSize()];
        xts.processDataUnit(plainTextByte, 0, producedCipherTextByte, 0, dataUnitNumber);
	}
	
	private static void decryptXTSMode() {
		
		setAESMode(false);
		
		//Set up XTS mode
		XTS xts = new XTS(cipher, tweakCipher);
        cipherTextByte = ByteUtil.hexToBytes(cipherText);
        producedPlainTextByte = new byte[xts.getDataUnitSize()];
        xts.processDataUnit(cipherTextByte, 0, producedPlainTextByte, 0, dataUnitNumber);
	}
	
	private static void setAESMode(boolean encrypt) {
		//Initialize AES with encryption mode
		// True: encrypt False: decrypt
		cipher.init(encrypt, keyParam);
	}

}
