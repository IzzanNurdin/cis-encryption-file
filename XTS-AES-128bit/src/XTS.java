import java.util.Arrays;

import org.bouncycastle.crypto.BlockCipher;

import util.ByteUtil;

public class XTS {
	
	private BlockCipher cipher, tweakCipher;
	private int blockSize;
	private final int dataUnitSize = 512;
	private final int longSize = 8;
	
	public XTS(BlockCipher cipher, BlockCipher tweakCipher) {
		this.cipher = cipher;
		this.tweakCipher = tweakCipher;
		this.blockSize = cipher.getBlockSize();
	}
	
	public void processDataUnit(byte[] input, int inOffset, byte[] output, int outOffset, long dataUnitNumber) {
		byte[] tweak = new byte[blockSize];
		ByteUtil.storeInt64LE(dataUnitNumber, tweak, 0);
		Arrays.fill(tweak, longSize, blockSize, (byte)0);
		this.tweakCipher.processBlock(tweak, 0, tweak, 0);
		for (int i = 0; i < dataUnitSize; i += blockSize)
		{
			// Encrypt / decrypt one block
			processBlock(input, inOffset + i, output, outOffset + i, tweak);
			// Multiply tweak by alpha
			tweak = multiplyTweakByA(tweak);
		}
	}
	
	private void processBlock(byte[] input, int inOffset, byte[] output, int outOffset, byte[] tweak)
	{
		for (int i = 0; i < blockSize; i++)
			input[inOffset + i] ^= tweak[i];

		cipher.processBlock(input, inOffset, output, outOffset);

		for (int i = 0; i < blockSize; i++)
			output[outOffset + i] ^= tweak[i];
	}
	
	private byte[] multiplyTweakByA(byte[] tweak)
	{
		long whiteningLo = ByteUtil.loadInt64LE(tweak, 0);
		long whiteningHi = ByteUtil.loadInt64LE(tweak, longSize);

		int finalCarry = 0 == (whiteningHi & 0x8000000000000000L) ? 0 : 135;
		
		whiteningHi <<= 1;
		whiteningHi |= whiteningLo >>> 63;
		whiteningLo <<= 1;
		whiteningLo ^= finalCarry;
		
		ByteUtil.storeInt64LE(whiteningLo, tweak, 0);
		ByteUtil.storeInt64LE(whiteningHi, tweak, longSize);
		
		return tweak;
	}	

	public int getDataUnitSize() {
		// TODO Auto-generated method stub
		return dataUnitSize;
	}	
}
